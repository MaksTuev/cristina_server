package domain;

import play.Logger;

import javax.persistence.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import static javax.persistence.GenerationType.*;

/**
 * Пользователь системы
 */
@Entity
@Table(name = "User")
public class User {

    @Id @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private int id = 0;

    @Column(name = "login")
    private String login = "";

    @Column(name = "password")
    private String password;

    @Column(name = "group")
    private int group;

    private static String salt = "50a314209c54751015d5ea87733dbccc98864d31";

    public User() {
        this.id       = 0;
        this.login    = null;
        this.password = null;
        this.group    = 0;
    }

    /**
     * @param login    Логин
     * @param password Пароль
     */
    public User(String login, String password) throws NoSuchAlgorithmException {
        this.id       = 0;
        this.login    = login;
        this.password = User.password(password);
        this.group    = 0;
    }

    @Override
    public String toString() {
        return String.format("Object=%s id=%05d login=%s group=%s", super.toString(), id, login, group);
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }

        if (object == null || getClass() != object.getClass()) {
            return false;
        }

        User user = (User) object;

        if (id != user.id) {
            return false;
        }

        if (!login.equals(user.login)) {
            return false;
        }

        boolean result;

        try {
            result = password.equals(password(user.password));
        } catch (NoSuchAlgorithmException exception) {
            Logger.error(exception.getMessage(), exception);
            result = false;
        }

        return result;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + login.hashCode();
        result = 31 * result + password.hashCode();
        return result;
    }

    /**
     * Получение хэша пароля
     *
     * @param password Исходный пароль
     * @return Хэш пароля
     */
    public static String password(String password) throws NoSuchAlgorithmException {
        MessageDigest digest = MessageDigest.getInstance("SHA-1");
        digest.reset();
        digest.update((password + User.salt).getBytes());

        byte[] hash = digest.digest();

        StringBuilder buffer = new StringBuilder();
        for (byte value : hash) {
            buffer.append(Integer.toString((value & 0xff) + 0x100, 16).substring(1));
        }

        return buffer.toString();
    }

    public int getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public int getGroup() {
        return group;
    }

    public boolean isLogged() {
        return this.id != 0;
    }
}
