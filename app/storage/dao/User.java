package storage.dao;

import java.sql.SQLException;
import java.util.List;

public interface User {

    void create(domain.User user) throws SQLException;
    void update(domain.User user) throws SQLException;
    void delete(domain.User user) throws SQLException;

    domain.User read(Integer id) throws SQLException;
    List read() throws SQLException;
}
