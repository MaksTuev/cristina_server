package storage;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class Hibernate {

    private static Hibernate instance = null;
    private SessionFactory sessionFactory;

    private Hibernate() {
        this.sessionFactory = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
    }

    public static Hibernate getInstance() {
        if (Hibernate.instance == null) {
            Hibernate.instance = new Hibernate();
        }
        return Hibernate.instance;
    }

    public SessionFactory getSessionFactory() {
        return this.sessionFactory;
    }
}
