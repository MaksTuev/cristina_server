package storage;

import storage.dao.implementation.UserDAO;

public class Factory {

    private static Factory instance = new Factory();
    private UserDAO userDAO;

    public static synchronized Factory getInstance() {
        return instance;
    }

    private Factory() {
        this.userDAO = new UserDAO();
    }

    public UserDAO getUserDAO() {
        return this.userDAO;
    }
}
