package controllers;

import play.mvc.*;

public class Patients extends Controller {

    @Security.Authenticated(Secured.class)
    public Result index() {
        return ok("Patients");
    }
}
