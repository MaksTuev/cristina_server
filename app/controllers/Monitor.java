package controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;

import domain.audio.AudioSampleReader;
import domain.metric.*;
import domain.remote.SessionService;

import play.libs.Json;
import play.mvc.*;
import views.html.Monitor.devices;
import views.html.Monitor.graphics;

import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Map;

public class Monitor extends Controller {

    final int NO_SOUNDBLOCK_AVAILABLE = 10;

    private static class StreamMapper {
        public String id;
        public int size;

        public StreamMapper(String id, int size) {
            this.id = id;
            this.size = size;
        }
    }

    @Security.Authenticated(Secured.class)
    public Result index() throws NoSoundStreamAvailableException {
        return ok(graphics.render());
    }

    /**
     * Подключенные АС
     */
    @Security.Authenticated(Secured.class)
    public Result devices() {
        return ok(devices.render(SessionService.getInstance().getServers()));
    }

    /**
     * Получить список доступных потоков
     */
    @Security.Authenticated(Secured.class)
    public Result streams() {
        ObjectNode json = Json.newObject();

        ArrayNode list = json.putArray("streams");
        ObjectMapper mapper = new ObjectMapper();

        for (Map.Entry<String, SoundStream> pair : MetricService.getInstance().getStreams().entrySet()) {
            ObjectNode node = mapper.valueToTree(new StreamMapper(pair.getKey(), pair.getValue().size()));
            list.add(node);
        }

        return ok(json);
    }

    /**
     * Получение блока данных
     */
    @Security.Authenticated(Secured.class)
    public Result block() {
        String stream;
        int block, scale;
        ObjectNode json = Json.newObject();

        try {
            stream = request().getQueryString("stream");
            block  = Integer.parseInt(request().getQueryString("block"));
            scale  = Integer.parseInt(request().getQueryString("scale"));
        } catch (NumberFormatException exception) {
            json.put("status", "error").put("message", "Проверьте параметры запроса. Ожидаются поля stream, block, scale");
            return ok(json);
        }

        double[] result;

        SoundStream soundStream;

        try {
            soundStream = MetricService.getInstance().getStream(stream);
            result = soundStream.scale(scale).getBlock(block);
        } catch (NoSoundStreamAvailableException exception) {
            json.put("status", "error").put("message", String.format("Поток %s недоступен", stream));
            return ok(json);
        } catch (NoSoundBlockAvailableException exception) {
            json
                .put("status", "error")
                .put("code", NO_SOUNDBLOCK_AVAILABLE)
                .put("message", String.format("Блок %d недоступен", block));
            return ok(json);
        }

        ObjectMapper mapper = new ObjectMapper();
        ArrayNode listNode = mapper.valueToTree(result);

        json
            .put("status", "success")
            .put("size", soundStream.size())
            .putArray("block").addAll(listNode);

        return ok(json);
    }

    public Result addTestStream() {
        try {
            AudioSampleReader sampleReader = new AudioSampleReader(new File("heartbeat.wav"));

            long nbSamples = sampleReader.getSampleCount();
            double[] samples = new double[(int)nbSamples];
            sampleReader.getInterleavedSamples(0, nbSamples, samples);

            for (int i = 0; i < samples.length / SoundBlock.DEFAULT_SIZE; i++) {
                SoundBlock block;

                double[] buffer = Arrays.copyOfRange(samples, i * SoundBlock.DEFAULT_SIZE, (i + 1) * SoundBlock.DEFAULT_SIZE);
                block = new SoundBlock(i, buffer);

                MetricService.getInstance().prepareStream("1").addBlock("1", block);
            }

            return ok("Поток добавлен");
        } catch (UnsupportedAudioFileException|IOException exception) {
            System.err.println(exception);
            return ok(exception.getMessage());
        }
    }
}
