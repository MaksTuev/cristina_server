package controllers;

import domain.metric.MetricService;
import domain.remote.SessionService;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;

public class Api extends Controller {

    @Security.Authenticated(Secured.class)
    public Result cleanStreams() {
        MetricService.getInstance().clean();
        return ok("Потоки очищены");
    }

    @Security.Authenticated(Secured.class)
    public Result cleanSessions() {
        SessionService.getInstance().clean();
        return ok("Сессии очищены");
    }
}
